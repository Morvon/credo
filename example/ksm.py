from typing import List, Tuple, Union, Callable

from features import display, display_all_from
from credo_cf import group_by_id, nkg_mark_hit_area, save_json, find_all_maximums, NKG_MASK, GRAY
import numpy as np


def run(objects, used_hits, quantity: int = 20):
    hits = []
    by_id = group_by_id(objects)
    if len(used_hits) < quantity: quantity = len(used_hits)
    counter = 0
    for u in used_hits:
        hits.append(by_id[u][0])
        if counter >= quantity:
            break
        counter += 1
    for h in hits:
        nkg_mark_hit_area(h)
        nkg_mark_hit_area_v2(h)
        nkg_mark_hit_area_mod(h)
    display_all_from(hits, 'gray')
    display_all_from(hits, NKG_MASK)
    display_all_from(hits, 'nkg_mask_with_avg')
    # display_all_from(hits, 'outline_on_grey')
    # display_all_from(hits, 'mark')
    # display_all_from(hits, 'cut_hit_mark')
    display_all_from(hits, 'final')
    # display_all_from(hits, 'finalv2')  # nkg for hit


def nkg_mark_hit_area_v2(detection: dict, recursion_func=None, kernel: List[Tuple[int, int]] = None,
                         mask_method: Union[str, Callable[[List[int]], np.ndarray]] = 'derivate'):
    detection['ksm_avg'] = universal_filter_ksm(detection, mask=AVERAGING_FILTER)
    ret = find_all_maximums(detection.get('ksm_avg'), recursion_func=recursion_func, kernel=kernel,
                            spread_on_flat=False)
    find_median = ret['values']

    if mask_method in ['derivate', 'derivate2']:
        fm = list(reversed(find_median))
        derivates = []
        for i in range(1, len(fm)):
            derivates.append(fm[i] - fm[i - 1])

        derivates2 = []
        for i in range(1, len(derivates)):
            derivates2.append(int(derivates[i]) - int(derivates[i - 1]))

        for i in range(0, len(derivates)):
            if derivates[i] > 2:
                threshold = fm[i] + 1
                break

    elif mask_method == 'median':
        core_median = find_median[len(find_median) // 2]
        # threshold = (find_median[0] - core_median) // 10 + core_median
        threshold = core_median + 5

    ret2 = find_all_maximums(detection.get(GRAY), recursion_func=recursion_func, kernel=kernel, spread_on_flat=True,
                             threshold=threshold)
    detection['nkg_mask_with_avg'] = np.where(ret2['mask'] > 0, 1, 0)

    return detection


def nkg_mark_hit_area_mod(detection: dict, recursion_func=None, kernel: List[Tuple[int, int]] = None):
    detection['ksm_avg'] = universal_filter_ksm(detection, mask=AVERAGING_FILTER)
    ret = find_all_maximums(detection['ksm_avg'], recursion_func=recursion_func, kernel=kernel, spread_on_flat=False,
                            max_maximums=200)
    find_median = ret['values']
    core_median = find_median[len(find_median) // 2]
    # threshold = (find_median[0] - core_median) // 10 + core_median
    threshold = core_median + 2
    ret2 = find_all_maximums(detection.get(GRAY), recursion_func=recursion_func, kernel=kernel, spread_on_flat=True,
                             threshold=threshold, max_maximums=200)
    detection['nkg_mask_mod'] = ret2['mask']

    detection['ksm_mask_avg'] = universal_filter_ksm(detection, 'nkg_mask_mod', mask=AVERAGING_FILTER)
    for i in range(0, 2):
        detection['ksm_mask_avg'] = universal_filter_ksm(detection, 'ksm_mask_avg', mask=AVERAGING_FILTER)

    detection['mark'] = mark_all(detection['ksm_mask_avg'], treshold=5)
    detection['cut_hit_mark'] = cut_hit_in_mark(detection.get(GRAY), detection['mark'])
    threshold = set_treshold(detection.get('cut_hit_mark'))
    ret2 = find_all_maximums(detection.get('cut_hit_mark'), recursion_func=recursion_func, kernel=kernel,
                             spread_on_flat=True, threshold=threshold, max_maximums=200)
    detection['final'] = ret2['mask']

    return detection


def cut_hit_in_mark(ret, ret_mark):
    l = len(ret)
    new_ret = []
    for i in range(0, l):
        list_tmp = []
        for j in range(0, l):
            if ret_mark[i][j] == -55:
                list_tmp.append(ret[i][j])
            else:
                list_tmp.append(0)
        new_ret.append(list_tmp)
    new_ret = np.array(new_ret)
    return new_ret


def mark_all(ret, treshold: int = 0):
    l = len(ret)
    new_ret = []
    for i in range(0, l):
        list_tmp = []
        for j in range(0, l):
            if ret[i][j] > treshold:
                list_tmp.append(-55)
            else:
                list_tmp.append(0)
        new_ret.append(list_tmp)
    new_ret = np.array(new_ret)
    return new_ret


def set_treshold(ret) -> int:
    """
        FIXME
    """
    (unique, counts) = a = np.unique(ret, return_counts=True)
    if a[0][0] == 0:
        unique = unique[1:]
        counts = counts[1:]
    ilosc_pixeli = sum(counts)
    # najwięcej_pixeli = max(counts)
    pozycja_w_liscie = np.where(
        counts == max(counts))  # średnia dla szum, potencjalnie, lecz co w przypadku dużego suzmu?
    zakres = 4
    number = unique[pozycja_w_liscie[0][0]] + zakres
    return number


def view_all_one_by_one(objects, used_hits: set = None):
    by_id = group_by_id(objects)
    counter = 1
    counter_working = 0
    hits = []

    if used_hits is None:
        for i in hits:
            hits.append(i)
    else:
        for u in used_hits:
            hits.append(by_id[u][0])

    for h in hits:
        if counter_working % 50 == 0:
            print("{}".format(counter_working / len(objects)))
        # print("now {}".format(h['id']))
        nkg_mark_hit_area(h)
        # nkg_mark_hit_area_mod(h)
        counter_working += 1
    problematic_hits = []
    hits_to_again_view = []
    for h in hits:
        print("--------------------")
        display(h.get('gray'))
        # display(h.get('nkg_mask_mod'))
        # display(h.get('outline_on_grey'))
        problem = input("[{}] ID: {} problem outlines Y/T, show R: ".format(counter, h.get('id')))
        counter = counter + 1
        if problem.upper() == "Y" or problem.upper() == "T":
            problematic_hits.append({'hit_id': h['id']})
            print("added hit")
        if problem.upper() == "R":
            hits_to_again_view.append(h)
            print("added hit to again show")
        print("--------------------")
    if hits_to_again_view:
        pr = again(hits_to_again_view)
        for i in pr:
            problematic_hits.append(i)

    print(problematic_hits)
    print(hits_to_again_view)
    save_json(problematic_hits, "data/problematicV2.json")


def again(hits, pr: list = []):
    hits_to_again_view = []
    problematic_hits = []
    counter = 1
    for h in hits:
        print("--------------------")
        display(h.get('gray'))
        display(h.get('final'))
        problem = input("[{}] ID: {} problem outlines Y/T, show R: ".format(counter, h.get('id')))
        counter = counter + 1
        if problem.upper() == "Y" or problem.upper() == "T":
            problematic_hits.append({'hit_id': h['id']})
            print("added hit")
        if problem.upper() == "R":
            hits_to_again_view.append(h)
            print("added hit to again show")
        print("--------------------")
    print(problematic_hits)
    print(hits_to_again_view)
    if not hits_to_again_view:
        return pr
    else:
        return again(hits_to_again_view, pr)


HIGH_PASS_FILTRER = [[-1, -1, -1],
                     [-1, 9, -1],
                     [-1, -1, -1]]
MY_HIGH_PASS_FILTRER = [[-1, -1, -1],
                        [-1, 20, -1],
                        [-1, -1, -1]]

AVERAGING_FILTER = [[1, 1, 1],
                    [1, 1, 1],
                    [1, 1, 1]]

AVERAGING_FILTER_V2 = [[1, 1, 1],
                       [1, 2, 1],
                       [1, 1, 1]]
HP2 = [[1, -2, 1],
       [-2, 5, -2],
       [1, -2, 1]
       ]


def universal_filter_ksm(detection: dict, source='gray', mask=None):
    if mask is None:
        mask = HIGH_PASS_FILTRER
    ret = detection.get(source)
    side_size = int(len(mask) / 2)
    size = len(mask)
    length = len(ret[0])
    list_tmp = []
    new_ret = []
    # lewy róg
    list_tmp.append(int((ret[0][0] * mask[1][1] + ret[0][1] * mask[1][2] + ret[1][0] * mask[2][1] + ret[1][1] *
                         mask[2][2]) / 4))
    ####góra
    for i in range(1, length - side_size):
        list_tmp.append(int((ret[0][i] * mask[1][1] +
                             ret[0][i - 1] * mask[1][0] +
                             ret[0][i + 1] * mask[1][2] +
                             ret[1][i] * mask[2][1] +
                             ret[1][i - 1] * mask[2][0] +
                             ret[1][i + 1] * mask[2][2]) / 6))
    ####prawy róg
    list_tmp.append(int(
        (ret[0][-1] * mask[1][1] +
         ret[0][-2] * mask[1][0] +
         ret[1][-1] * mask[2][1] +
         ret[1][-2] * mask[2][0]) / 4))

    new_ret.append(list_tmp)
    list_tmp = []
    for i in range(side_size, length - side_size):
        # list_tmp.append(ret[i][0])        #przystosować do side_size!! dziala tylko na 3x3
        # list_tmp.append(0)
        list_tmp.append(int((ret[i][0] * mask[1][1] +
                             ret[i][1] * mask[1][2] +
                             ret[i + 1][0] * mask[0][1] +
                             ret[i + 1][1] * mask[0][2] +
                             ret[i - 1][0] * mask[2][1] +
                             ret[i - 1][1] * mask[2][2]) / 6))
        for j in range(side_size, length - side_size):
            counter = 0
            sum = 0
            ii = i - 1
            jj = j - 1
            for k in range(0, size):
                for l in range(0, size):
                    if mask[k][l] != 0: counter += 1
                    sum += mask[k][l] * ret[ii][jj]
                    jj = jj + 1
                ii = ii + 1
                jj = j - 1
            avg = int(sum / (size * size))
            list_tmp.append(avg)
        # list_tmp.append(ret[i][length-1])        #przystosować do side_size!! dzila tylko na 3x3
        # list_tmp.append(0)
        list_tmp.append(int((ret[i][-1] * mask[1][1] +
                             ret[i][-2] * mask[1][0] +
                             ret[i + 1][-1] * mask[0][1] +
                             ret[i + 1][-2] * mask[0][0] +
                             ret[i - 1][-1] * mask[2][1] +
                             ret[i - 1][-2] * mask[2][0]) / 6))
        new_ret.append(list_tmp)
        list_tmp = []
    # new_ret.append(ret[-1])
    list_tmp = []
    # lewy róg dolny
    list_tmp.append(int((ret[-1][0] * mask[1][1] + ret[-1][1] * mask[1][2] + ret[-2][0] * mask[0][1] +
                         ret[-2][1] * mask[0][2]) / 4))
    ####dół
    for i in range(1, length - side_size):
        list_tmp.append(int((ret[-1][i] * mask[1][1] +
                             ret[-1][i - 1] * mask[1][0] +
                             ret[-1][i + 1] * mask[1][2] +
                             ret[-2][i] * mask[0][1] +
                             ret[-2][i - 1] * mask[0][0] +
                             ret[-2][i + 1] * mask[0][2]) / 6))
    ####prawy róg dolny
    list_tmp.append(int(
        (ret[-1][-1] * mask[1][1] +
         ret[-1][-2] * mask[1][0] +
         ret[-2][-1] * mask[0][1] +
         ret[-2][-2] * mask[0][0]) / 4))
    new_ret.append(list_tmp)
    newRetNp = np.array(new_ret)
    return newRetNp


def apply_outline(ret_gray, ret_mod):
    l = len(ret_gray)
    new_ret = []
    for i in range(0, l):
        list_tmp = []
        for j in range(0, l):
            if ret_mod[i][j] == -10:
                list_tmp.append(-10)
            else:
                list_tmp.append(ret_gray[i][j])
        new_ret.append(list_tmp)
    new_ret = np.array(new_ret)
    return new_ret


def find_outline(ret, pivot: int = -10):
    l = len(ret) - 1
    new_ret = []
    list_tmp = []
    if ret[0][0] != 0:
        if ret[0][1] == 0 or ret[1][1] == 0 or ret[1][0] == 0:
            list_tmp.append(pivot)
        else:
            list_tmp.append(ret[0][0])
    else:
        list_tmp.append(0)

    for i in range(1, l):
        if ret[0][i] == 0:
            list_tmp.append(0)
        elif ret[0][i - 1] == 0 or ret[0][i + 1] == 0 or ret[0][i - 1] == 0 or \
                ret[1][i - 1] == 0 or ret[1][i] == 0 or ret[1][i + 1] == 0:
            list_tmp.append(pivot)
        else:
            list_tmp.append(ret[0][i])

    if ret[0][-1] != 0:
        if ret[0][-2] == 0 or ret[1][-2] == 0 or ret[1][-1] == 0:
            list_tmp.append(pivot)
        else:
            list_tmp.append(ret[0][-1])
    else:
        list_tmp.append(0)

    new_ret.append(list_tmp)
    list_tmp = []
    # sprawdza gdzie jest granica i zamienia ją na -10
    for i in range(1, l):
        # list_tmp.append(0)
        if ret[i][0] != 0:
            if ret[i][1] == 0 or ret[i + 1][0] == 0 or ret[i - 1][0] == 0 \
                    or ret[i + 1][1] == 0 or ret[i - 1][1] == 0:
                list_tmp.append(pivot)
            else:
                list_tmp.append(ret[i][0])
        else:
            list_tmp.append(0)

        for j in range(1, l):
            if ret[i][j] == 0:
                list_tmp.append(0)
            elif ret[i][j - 1] == 0:
                list_tmp.append(pivot)
            elif ret[i][j + 1] == 0:
                list_tmp.append(pivot)
            elif ret[i - 1][j - 1] == 0:
                list_tmp.append(pivot)
            elif ret[i - 1][j + 1] == 0:
                list_tmp.append(pivot)
            elif ret[i + 1][j - 1] == 0:
                list_tmp.append(pivot)
            elif ret[i + 1][j + 1] == 0:
                list_tmp.append(pivot)
            elif ret[i - 1][j] == 0:
                list_tmp.append(pivot)
            elif ret[i + 1][j] == 0:
                list_tmp.append(pivot)
            else:
                list_tmp.append(ret[i][j])
        # list_tmp.append(0)
        if ret[i][-1] != 0:
            if ret[i][-2] == 0 or ret[i + 1][-1] == 0 or ret[i - 1][-1] == 0 \
                    or ret[i + 1][-2] == 0 or ret[i - 1][-2] == 0:
                list_tmp.append(pivot)
            else:
                list_tmp.append(ret[i][-1])
        else:
            list_tmp.append(0)
        new_ret.append(list_tmp)
        list_tmp = []

    # newRet.append([0 for i in range(0, 60)])
    list_tmp = []
    if ret[-1][0] != 0:
        if ret[-1][1] == 0 or ret[-2][1] == 0 or ret[-2][0] == 0:
            list_tmp.append(pivot)
        else:
            list_tmp.append(ret[-1][0])
    else:
        list_tmp.append(0)

    for i in range(1, l):
        if ret[-1][i] == 0:
            list_tmp.append(0)
        elif ret[-1][i - 1] == 0 or ret[-1][i + 1] == 0 or ret[-1][i - 1] == 0 or \
                ret[-2][i - 1] == 0 or ret[-2][i] == 0 or ret[-2][i + 1] == 0:
            list_tmp.append(pivot)
        else:
            list_tmp.append(ret[-1][i])

    if ret[-1][-1] != 0:
        if ret[-1][-2] == 0 or ret[-2][-2] == 0 or ret[-2][-1] == 0:
            list_tmp.append(pivot)
        else:
            list_tmp.append(ret[-1][-1])
    else:
        list_tmp.append(0)
    new_ret.append(list_tmp)
    new_ret = np.array(new_ret)
    return new_ret


@DeprecationWarning
def adaptive_reduce_value_ksm(_max, _min, new_ret_np, ret_clear):
    counter = 0
    """
    History:
    15 dużo błędnych
    10 Mniej błędnych
    8 jeszcze mniej błędnych
    5,6 nie znajduje już tych złych
    7-8 przy SENSTIVE 5 i filtrze -1 -> 20 najlepiej
    """
    ace = 7

    max_new = 0
    min_new = 0
    counter_hit = 0
    for i in ret_clear:
        for j in i:
            if j + ace > _min > j - ace and j != 0:
                counter += 1
            if j > 0: counter_hit += 1
            if j > max_new: max_new = j
            if min_new > j > 0 or min_new <= 0: min_new = j
    dis = _max - _min

    SENSITIVE = 8  # uzależnić od wielkości śladu..
    if dis != 0 and dis < _max and dis > _min and counter > SENSITIVE:
        list_tmp = []
        new_ret = []
        for i in new_ret_np:
            for j in i:
                # THR = j / dis
                if j < 0:
                    list_tmp.append(0)
                else:
                    list_tmp.append(int(j * (1 - (j) / (2 * dis))))
            new_ret.append(list_tmp)
            list_tmp = []
        new_ret_np = np.array(new_ret)
    else:
        rr = []
        for i in ret_clear:
            list_tmp = []
            for j in i:
                list_tmp.append(j)
            rr.append(list_tmp)
        new_ret_np = np.array(rr)
    return new_ret_np
