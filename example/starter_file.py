import sys
from credo_cf import progress_and_process_image, load_json, group_by_id
from ksm import run, nkg_mark_hit_area_mod
from json import loads
from ksm_module import measure_angle_ksm


# view_all_one_by_one(objects)

def main_old():
    objects, count, errors = load_json('../data/manual.json', progress_and_process_image)
    used_hits = set()
    with open('data/problematic.json') as f:
        data_ = loads(f.read())
        for hit in data_['detections']:
            used_hits.add(hit['hit_id'])
    run(objects, used_hits=used_hits)


def run_angle_ksm(fn: str, used_hits_id=None, post_fix=""):

    hits, count, errors = load_json('../data/%s' % fn, progress_and_process_image)
    used_hits = hits
    if used_hits_id is not None:
        used_hits = []
        by_id = group_by_id(hits)
        for u in used_hits_id:
            used_hits.append(by_id[u][0])
    measure_angle_ksm(hits=used_hits, fn=fn,
                      post_fix=post_fix,
                      processing_mark=(nkg_mark_hit_area_mod, 'final'))


def main():
    # print(find_the_best_points([[26, 26], [26, 27]], [[32, 32], [33, 33]]))
    # print(find_the_best_points([[26, 20],
    # [27, 20]], [[26, 30], [25, 30]]))
    # sort_with_path([(33, 28), (32, 29), (31, 30), (30, 30), (29, 31), (28, 32),
    # (27, 33), (26, 34), (25, 35), (24, 36)])
    # measure_angle_ksm('hits_votes_4_class_2.json', [4640374, 5192876])
    # measure_angle_ksm('hits_votes_4_class_3.json')
    # find_the_best_points_in_all(
    #    [(31, 39), (31, 38), (31, 37), (31, 36), (31, 35), (30, 34), (30, 33), (30, 32), (30, 31), (30, 30), (31, 30),
    #     (32, 30), (33, 31), (33, 32), (33, 33)])
    # full_two_pixel_path([(26, 26), (26, 27), (25, 28), (26, 29), (26, 30)]) # lewo prawo z wyskokiem pixela #Git
    # full_two_pixel_path([(30,30),(31,31),(32,32),(33,32),(33,33),(34,34),(35,35)]) # na ukos z wyskokiem # git
    # full_two_pixel_path([(26,25), (26, 26), (26, 27), (25, 28), (25, 29), (24, 30),(24,31),(24,32)]) # lewo prawo z przesunieciem dwupikselowym
    # measure_angle_ksm_with_two_pix('hits_votes_4_class_3.json', [17355208], post_fix="check_straight_line_2_20procent_more_than0")
    # measure_angle_ksm_with_two_pix('hits_votes_4_class_2.json', [17576039], post_fix='test')

    #run_angle_ksm('hits_votes_4_class_3.json', post_fix="test_official_v8")
    #run_angle_ksm('hits_votes_4_class_2.json', post_fix="test_official_v8")
    #run_angle_ksm('hits_votes_4_class_2.json', used_hits_id=[4976474], post_fix="testy")
    #run_angle_ksm('hits_votes_4_class_2.json', used_hits_id=[5103534], post_fix="testy")
    #run_angle_ksm('hits_votes_4_class_3.json', used_hits_id=[9284922,9545280], post_fix="testy")
    run_angle_ksm('hits_votes_4_class_3.json', used_hits_id=[16829000], post_fix="testy")

if __name__ == '__main__':
    main()
    wrong_path = [4911378, 17369025, 17473206, 16295230, 5486815]
    szum = [17871453, 17589658, 17788929, 4964175, 5232884, 17639733, 18314581, 5035910, 5035910, 18314581, 17639733,
            4923639]
    # 2pixel path usually fix this
    inne = [4731298, 4595686, 4612613, 4983039, 6998185, 8461320, 17368987,
            17776650, 4847592, 4847592, 16294165, 17368475, 17368786,
            17369034,
            17369076,
            17371284,
            17513004,
            17584627,
            17699984,
            17705067,
            17769520,
            17911036,
            ]
    sys.exit(0)  # not always close
