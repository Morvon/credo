from credo_cf import group_by_id, save_json
from example.features import display
from ksm_module.preprocess.ksm_processing import ksm_mark_hit_area


def view_all_one_by_one(objects, used_hits: set = None):
    by_id = group_by_id(objects)
    counter = 1
    counter_working = 0
    hits = []

    if used_hits is None:
        for i in hits:
            hits.append(i)
    else:
        for u in used_hits:
            hits.append(by_id[u][0])

    for h in hits:
        if counter_working % 50 == 0:
            print("{}".format(counter_working / len(objects)))
        # print("now {}".format(h['id']))
        ksm_mark_hit_area(h)
        # nkg_mark_hit_area_mod(h)
        counter_working += 1
    problematic_hits = []
    hits_to_again_view = []
    for h in hits:
        print("--------------------")
        display(h.get('gray'))
        # display(h.get('nkg_mask_mod'))
        # display(h.get('outline_on_grey'))
        problem = input("[{}] ID: {} problem outlines Y/T, show R: ".format(counter, h.get('id')))
        counter = counter + 1
        if problem.upper() == "Y" or problem.upper() == "T":
            problematic_hits.append({'hit_id': h['id']})
            print("added hit")
        if problem.upper() == "R":
            hits_to_again_view.append(h)
            print("added hit to again show")
        print("--------------------")
    if hits_to_again_view:
        pr = again(hits_to_again_view)
        for i in pr:
            problematic_hits.append(i)

    print(problematic_hits)
    print(hits_to_again_view)
    save_json(problematic_hits, "data/problematicV2.json")


def again(hits, pr: list = []):
    hits_to_again_view = []
    problematic_hits = []
    counter = 1
    for h in hits:
        print("--------------------")
        display(h.get('gray'))
        display(h.get('final'))
        problem = input("[{}] ID: {} problem outlines Y/T, show R: ".format(counter, h.get('id')))
        counter = counter + 1
        if problem.upper() == "Y" or problem.upper() == "T":
            problematic_hits.append({'hit_id': h['id']})
            print("added hit")
        if problem.upper() == "R":
            hits_to_again_view.append(h)
            print("added hit to again show")
        print("--------------------")
    print(problematic_hits)
    print(hits_to_again_view)
    if not hits_to_again_view:
        return pr
    else:
        return again(hits_to_again_view, pr)