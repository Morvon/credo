import collections
from typing import List, Dict, Set

# STEP 1
from ksm_module.path.common import perpendicular_directions, STRAIGHT


def find_straight_lines(points_e: List[Dict]) -> List[Set]:
    """
    points_e - result from extend_path_information
    return: returns a list of sets, each set has start - stop indexes corresponding to points_e.
            Each set marks a straight line in the hit.
            It also means the number of all angles that the algorithm has detected, the angle is between lines
    """
    new_list = []
    actual_direction = points_e[0]['main_direction']
    starter_index = 0
    for idx, point in enumerate(points_e):
        if point['main_direction'] != actual_direction:
            new_list.append((starter_index, idx))
            starter_index = idx
            actual_direction = point['main_direction']
    new_list.append((starter_index, len(points_e) - 1))
    return new_list


# STEP 2
def set_directions(lines_idx: List[Dict], points_e: List[Dict]):
    """
    Adds direction to dictionary as "direction"
    points_e - result from extend_path_information
    lines_idx - dictionary with "point_idx"
    """
    for line_idx in lines_idx:
        line_idx['direction'] = points_e[line_idx['point_idx'][0]]['main_direction']
    return lines_idx


def set_directions_from_list_of_set(lines_idx: List[Set], points_e: List[Dict]):
    """
    Adaptation to overall function
    points_e - result from extend_path_information
    lines_idx - result from find_straight_lines
    """
    new_list = []
    for line_idx in lines_idx:
        new_list.append({"point_idx": line_idx})
    return set_directions(new_list, points_e)


# STEP 3
def merge_lines_with_hop(lines_idx):
    """
    Connects lines in one direction between which there is one hope e.g. N -> NS (jump) -> N => N
    lines_idx - dictionary with "point_idx" and "direction"
    """
    list_id = find_hop(lines_idx)

    if len(list_id) > 0:
        new_list = []
        added_points = False
        for idx, line_idx in enumerate(lines_idx):
            if added_points:
                added_points = False
                continue
            if idx == len(lines_idx) - 1 or idx == 0:
                new_list.append(line_idx)
                continue
            if idx in list_id:
                if lines_idx[idx - 1]['direction'] == lines_idx[idx + 1]['direction']:
                    if lines_idx[idx - 1] in new_list:
                        new_list.remove(lines_idx[idx - 1])
                        new_list.append({"point_idx": (lines_idx[idx - 1]['point_idx'][0],
                                                       lines_idx[idx + 1]['point_idx'][1]),
                                         "direction": lines_idx[idx + 1]['direction']})
                    else:
                        new_list.append({"point_idx": (lines_idx[idx - 1]['point_idx'][1],
                                                       lines_idx[idx + 1]['point_idx'][1]),
                                         "direction": lines_idx[idx + 1]['direction']})
                    added_points = True
                    continue
            new_list.append(line_idx)
        return new_list
    return lines_idx


def find_hop(lines_idx):
    new_list = []
    for idx, line_idx in enumerate(lines_idx):
        _point = line_idx['point_idx']
        if _point[1] - _point[0] == 1:
            new_list.append(idx)
    return new_list


def merge_custom_3_hop(lines_idx):
    list_id_hop = find_hop(lines_idx)
    len_hop = len(list_id_hop)
    if len_hop < 3:
        return lines_idx
    list_id_3points = []
    for i in range(0, len_hop - 2):
        if list_id_hop[i] + 1 == list_id_hop[i + 1] == list_id_hop[i + 2] - 1:
            list_id_3points.append(list_id_hop[i])
    if len(list_id_3points) > 0:
        new_list = []
        added_points = 0
        for idx, line_idx in enumerate(lines_idx):
            if added_points != 0:
                added_points -= 1
                continue
            if idx == len(lines_idx) - 1:
                new_list.append(line_idx)
                continue
            if idx in list_id_3points:
                short = lines_idx[idx:idx + 3]
                c = []
                for idx_2, i in enumerate(short):
                    if i['direction'] in STRAIGHT:
                        c.append(idx_2)
                if len(c) == 1:
                    good = True
                    for i in short:
                        if short[c[0]]['direction'] not in i['direction']:
                            good = False

                    if good:
                        new_list.append({"point_idx": (short[0]["point_idx"][0],
                                                       short[-1]["point_idx"][1]),
                                         "direction": short[c[0]]['direction']})
                        added_points = 2
                        continue
            new_list.append(line_idx)
    else:
        return lines_idx

    return new_list


def merge_adjacent_in_the_same_direction(lines_idx):
    if len(lines_idx) < 2:
        return lines_idx
    new_list = []
    idx_list = []
    direction = lines_idx[0]['direction']
    counter = 0
    for idx, dic in enumerate(lines_idx):
        if dic['direction'] == direction:
            counter += 1
            if idx == len(lines_idx) - 1:
                idx_list.append((idx - counter + 1, idx))
        else:
            if counter >= 2:
                idx_list.append((idx - counter, idx - 1))
            direction = dic['direction']
            counter = 1
    first = False
    _x = 0
    new_list_idx = [x for i in idx_list for x in i]
    for idx, (line_idx) in enumerate(lines_idx):
        point = line_idx['point_idx']
        if idx in new_list_idx:
            if first:
                first = False
                new_list.append({"point_idx": (_x, point[1]), "direction": line_idx['direction']})
            else:
                _x = point[0]
                first = True
        else:
            if not first:
                new_list.append(line_idx)
    return new_list


# Przed tym trzeba krańce zlikwidować
def merge_lines_with_hills(lines_idx):
    """
    we are looking for 2 jumps here
    Connects lines with hills, e.g. W -> NW -> SW -> W => W
    and situations where e.g. W -> NW -> SW -> NW => W -> NW
    situations where e.g W -> NW -> SW -> S => W -> S
    one hop to one, another to the other
    lines_idx - dictionary with "point_idx" and "direction"
    """
    list_id_hop = find_hop(lines_idx)
    len_hop = len(list_id_hop)
    if len_hop < 2:
        return lines_idx
    list_id_hills = []
    for i in range(0, len_hop - 1):
        if list_id_hop[i] + 1 == list_id_hop[i + 1]:
            list_id_hills.append(list_id_hop[i])

    if len(list_id_hills) > 0:
        new_list = []
        added_points = 0
        for idx, line_idx in enumerate(lines_idx):
            if added_points != 0:
                added_points -= 1
                continue
            if idx == len(lines_idx) - 1 or idx == 0:
                new_list.append(line_idx)
                continue
            if idx in list_id_hills:
                if lines_idx[idx - 1]['direction'] == lines_idx[idx + 2]['direction']:
                    if lines_idx[idx - 1] in new_list:
                        new_list.remove(lines_idx[idx - 1])
                        new_list.append({"point_idx": (lines_idx[idx - 1]['point_idx'][0],
                                                       lines_idx[idx + 2]['point_idx'][1]),
                                         "direction": lines_idx[idx + 2]['direction']})
                    else:
                        new_list.append({"point_idx": (lines_idx[idx - 1]['point_idx'][1],
                                                       lines_idx[idx + 2]['point_idx'][1]),
                                         "direction": lines_idx[idx + 2]['direction']})
                    added_points += 2
                    continue
                if lines_idx[idx - 1]['direction'] in lines_idx[idx + 2]['direction']:
                    if lines_idx[idx - 1] in new_list:
                        new_list.remove(lines_idx[idx - 1])
                        new_list.append({"point_idx": (lines_idx[idx - 1]['point_idx'][0],
                                                       lines_idx[idx + 1]['point_idx'][1]),
                                         "direction": lines_idx[idx - 1]['direction']})
                    else:
                        new_list.append({"point_idx": (lines_idx[idx - 1]['point_idx'][1],
                                                       lines_idx[idx + 1]['point_idx'][1]),
                                         "direction": lines_idx[idx - 1]['direction']})
                    added_points += 1
                    continue
                if lines_idx[idx - 1]['direction'] in perpendicular_directions(lines_idx[idx + 2]['direction']):
                    if lines_idx[idx - 1] in new_list:
                        new_list.remove(lines_idx[idx - 1])
                        new_list.append({"point_idx": (lines_idx[idx - 1]['point_idx'][0],
                                                       lines_idx[idx]['point_idx'][1]),
                                         "direction": lines_idx[idx - 1]['direction']})
                        new_list.append({"point_idx": (lines_idx[idx + 1]['point_idx'][0],
                                                       lines_idx[idx + 2]['point_idx'][1]),
                                         "direction": lines_idx[idx + 2]['direction']})
                    else:
                        new_list.append({"point_idx": (lines_idx[idx - 1]['point_idx'][1],
                                                       lines_idx[idx]['point_idx'][1]),
                                         "direction": lines_idx[idx]['direction']})
                        new_list.append({"point_idx": (lines_idx[idx + 1]['point_idx'][0
                                                       ],
                                                       lines_idx[idx + 2]['point_idx'][1]),
                                         "direction": lines_idx[idx + 2]['direction']})
                    added_points += 2
                    continue
            new_list.append(line_idx)
        return new_list
    return lines_idx


def merge_hop_at_the_end(lines_idx):
    list_id_hop = find_hop(lines_idx)
    if 0 in list_id_hop or len(lines_idx) - 1 in list_id_hop:
        new_list = []
        x = 0
        if 0 in list_id_hop:
            if len(lines_idx) - 1 in list_id_hop:
                new_list.append({"point_idx": (lines_idx[0]['point_idx'][0],
                                               lines_idx[1]['point_idx'][1]),
                                 "direction": lines_idx[1]['direction']})
            x = 2

        for i in range(x, len(lines_idx) - 3):
            new_list.append(lines_idx[i])
        if len(lines_idx) - 1 in list_id_hop:
            new_list.append({"point_idx": (lines_idx[-2]['point_idx'][0],
                                           lines_idx[-1]['point_idx'][1]),
                             "direction": lines_idx[-2]['direction']})
        else:
            new_list.append(lines_idx[-2])
            new_list.append(lines_idx[-1])
        return new_list
    return lines_idx


def merge_rest(lines_idx):
    list_id_hop = find_hop(lines_idx)
    if len(list_id_hop) > 0:
        new_list = []
        added_points = False
        for idx, line_idx in enumerate(lines_idx):
            if added_points:
                added_points = False
                continue
            if (idx + 1) in list_id_hop:
                new_list.append({'point_idx': (line_idx['point_idx'][0], lines_idx[idx + 1]['point_idx'][1]),
                                 'direction': line_idx['direction']})
                added_points = True
                continue
            new_list.append(line_idx)
        return new_list
    return lines_idx
