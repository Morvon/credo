import collections
import math
from credo_cf.classification.preprocess.nkg_processings import angle_3points
from ksm_module.path.common import first_point_which_is_good, SLANT, extend_direction, get_angle_math

#to rozbite w nowtym
def set_line_v2(list_of_point_index):
    new_list = []
    _start = 0
    _slant = False
    for idx, point in enumerate(list_of_point_index):
        if point[1] - point[0] >= 2:  # jest więcej niż 2 przeskoki -> 3 punkty
            if _slant:  # skos sie skonczyl
                if idx == len(list_of_point_index) - 1:
                    if len(new_list) == 0:
                        new_list.append((0, point[0]))
                        new_list.append(point)
                        continue
                    new_list.append((_start, point[1]))
                    continue
                new_list.append((_start, point[0]))
                _slant = False
            _start = point[1]
            new_list.append(point)
        else:
            _slant = True
            if idx == len(list_of_point_index) - 1:
                new_list.append((_start, point[1]))
    print(new_list)
    return new_list

#przeniesione
def set_line_v2_step2(list_of_point_index):
    """
    connects lines in one direction between which there is one hop
    """
    list_id = []
    for idx, point in enumerate(list_of_point_index):
        point = point['point_idx']
        if point[1] - point[0] == 1:
            list_id.append(idx)
    if len(list_id) > 0:
        new_list = []
        added_point = False
        for idx, point in enumerate(list_of_point_index):
            point = point['point_idx']
            if added_point:
                added_point = False
                continue
            if idx in list_id:  # to id jest w liscie do zlaczenia
                # Sekcja skrajnie lewy i prawy do przemyslenia, czy podłączyć je pod kreske
                # ale wydaje sie ze tak
                if idx == 0:  # skrajny lewy
                    new_list.append((point[0], list_of_point_index[idx + 1]['point_idx'][1]))
                elif idx == len(list_of_point_index) - 1:  # skrajny prawy
                    # usuwamy ostatni dodany, przedostatni w liscie
                    # bo bedziemy jego scalać
                    if list_of_point_index[idx - 1]['point_idx'] in new_list:
                        new_list.remove(list_of_point_index[idx - 1]['point_idx'])
                    # Teraz dodajemy nowy
                    new_list.append((list_of_point_index[idx - 1]['point_idx'][0], point[1]))
                else:
                    # jeżeli sytaucja linia, pixel, linia ,pixel
                    # nowa_linia, pixel i remove nie jest zawsze potrzebny
                    if list_of_point_index[idx - 1]['main_direction'] == list_of_point_index[idx + 1]['main_direction']:
                        if list_of_point_index[idx - 1]['point_idx'] in new_list:
                            new_list.remove(list_of_point_index[idx - 1]['point_idx'])
                            new_list.append((list_of_point_index[idx - 1]['point_idx'][0],
                                             list_of_point_index[idx + 1]['point_idx'][1]))
                        else:
                            new_list.append((list_of_point_index[idx - 1]['point_idx'][1],
                                             list_of_point_index[idx + 1]['point_idx'][1]))
                    else:
                        # będzie pomijanie przeskoku, ale punkty sie nie stracą tak sądzę,
                        added_point = False
                        continue
                        pass
                added_point = True
            else:
                new_list.append(point)
        return new_list
    else:
        return [x['point_idx'] for x in list_of_point_index]

#przebudowane
def set_line_v2_step3(list_of_point_index, points):
    """
    Adds the direction in which the straight line goes
    """
    new_list = []
    _x = list_of_point_index[-1][0]
    _y = list_of_point_index[-1][1]
    dd = 0
    for x, y in list_of_point_index:
        if x == _x and y == _y:
            dd = 1
        s_points = points[x:y + dd]  # y+1 weźmie kierunek ostatniego hita gdzie jest zmieniany
        counter = collections.Counter()
        for point in s_points:
            counter[point['main_direction']] += 1
        if len(counter) == len(s_points):  # kazdy punkt ma inny kierunek, trzeba na innej podstawie określić
            pass
        two = counter.most_common(2)
        ll = len(two)
        if ll > 1:
            xx = two[0][1]
            yy = two[1][1]
            if xx == yy:
                if xx == 1:
                    new_list.append({"point_idx": (x, y), "main_direction": two[0][0]})
                    continue
                if x == _x and y == _y:  # ostatni pixel, warunek zeby koncowka nie determinowala ogolnego kierunku
                    new_list.append({"point_idx": (x, y), "main_direction": two[0][0]})
                    continue
                if two[1][0] in SLANT:
                    new_list.append({"point_idx": (x, y), "main_direction": two[1][0]})
                    continue
        new_list.append({"point_idx": (x, y), "main_direction": counter.most_common(1)[0][0]})
    return new_list

#przeniesione
def set_line_v2_step4(idx_points):
    """
    joins two adjacent lines into one if they are in the same direction
    """
    if len(idx_points) < 2:
        return [{"point_idx": x['point_idx'], "direction": x['main_direction']} for x in idx_points]
    new_list = []
    idx_list = []
    direction = idx_points[0]['main_direction']
    counter = 0
    for idx, dic in enumerate(idx_points):
        if dic['main_direction'] == direction:
            counter += 1
            if idx == len(idx_points) - 1:
                # last
                # idx_list.append({"point": (idx - counter + 1, idx), "direction": direction})
                idx_list.append((idx - counter + 1, idx))
        else:
            if counter >= 2:
                # idx_list.append({"point": (idx - counter, idx - 1), "direction": direction})
                idx_list.append((idx - counter, idx - 1))
            direction = dic['main_direction']
            counter = 1
    first = False
    _x = 0
    for idx, (idx_point) in enumerate(idx_points):
        point = idx_point['point_idx']
        new_list_idx = [x for i in idx_list for x in i]
        if idx in new_list_idx:
            if first:
                first = False
                new_list.append({"point_idx": (_x, point[1]), "direction": idx_point['main_direction']})
            else:
                _x = point[0]
                first = True
        else:
            # nie ma wiec przepisujemy
            if not first:
                new_list.append({"point_idx": point, "direction": idx_point['main_direction']})
    return new_list


def set_line_v2_step5_ver1(line_index, points):
    len_index = len(line_index)
    if len_index < 2:
        return 180
    if len_index % 2 == 0:
        mid = len_index // 2
        idx_point = line_index[mid]['point_idx'][0]
        mid_point = points[idx_point]['point']
        a = points[0]['point']
        b = points[-1]['point']
        return angle_3points(a, mid_point, b) / math.pi * 180.0
    else:
        mid = len_index // 2
        line = line_index[mid]['point_idx']
        line_a = points[line[0]]['point']
        line_b = points[line[-1]]['point']
        a = points[line_index[mid - 1]['point_idx'][0]]['point']
        b = points[line_index[mid + 1]['point_idx'][-1]]['point']
        angle_first = angle_3points(a, line_a, line_b) / math.pi * 180.0
        angle_two = angle_3points(line_a, line_b, b) / math.pi * 180.0
        return angle_first + angle_two


def set_line_v2_step5_ver2():
    pass


EXPERIMENTAL = True


def set_line_v2_step5_ver3(lines_index, points):
    le = len(lines_index)
    if le < 2:
        return [180]
    if le == 2:  # dwie kreski więc można od razu liczyć kąt bez większego martwienia
        if EXPERIMENTAL:
            for line in lines_index:
                if line['point_idx'][1] - line['point_idx'][0] <= 3:
                    # w rzeczywistosci to beda dwa, ponieważ tutaj linie na siebie zachodzą
                    return [180]
        a = first_point_which_is_good(lines_index[0]["direction"],
                                      points[lines_index[0]['point_idx'][0]:lines_index[0]['point_idx'][1] + 1])

        # aaa = lines_index[1]['point_idx'][0]
        # bbb = lines_index[1]['point_idx'][1] + 1
        # cut_list = points[aaa:bbb]
        # tmp_list = [x for x in reversed(cut_list)]
        c = first_point_which_is_good(lines_index[1]["direction"], [x for x in reversed(
            points[lines_index[1]['point_idx'][0]: lines_index[1]['point_idx'][1] + 1]
        )])
        b = points[lines_index[0]['point_idx'][1]]['point']
        angle = angle_3points(a, b, c) / math.pi * 180.0
        if math.isnan(angle):
            # angle = get_angle_math(a, b, c)
            # Możliwe, że jak jest nan to jest kreska 180 z zakrzywieniem jakimś
            # do obserwacji taki wniosek
            # wszystkie powyżej 180 stopni kreski się wpisują w to
            # ponieważ algorytm wyznacza 3 punkty lecz 2 z nich sie pokrywają -> linia prosta
            angle = 180
        return [angle]
    else:
        (_type, properties) = advanced_analysis_S(lines_index)
        directions = properties['directions']
        if _type == 1:  # Pozaginany robak w jednym kierunku
            angles = []
            for idx, line_index in enumerate(lines_index):
                if idx == len(lines_index) - 1:
                    break
                a = points[line_index['point_idx'][0]]['point']
                b = points[line_index['point_idx'][1]]['point']
                c = points[lines_index[idx + 1]['point_idx'][1]]['point']
                angles.append(angle_3points(a, b, c) / math.pi * 180.0)

            return angles
        elif _type == 2:  # L/U, jeden kąt
            direction_find = directions[0]
            direction_find2 = directions[1]
            ix = 0
            ix2 = 0
            find_ix1 = find_ix2 = True
            for idx, point in enumerate(lines_index):
                if point['direction'] == direction_find and find_ix1:
                    ix = idx
                    find_ix1 = False
                if point['direction'] == direction_find2 and find_ix2:
                    ix2 = idx
                    find_ix2 = False
            # idx
            mid = lines_index[ix2]['point_idx'][0]
            a = lines_index[ix]['point_idx'][0]
            c = lines_index[-1]['point_idx'][-1]
            angle = angle_3points(points[a]['point'], points[mid]['point'], points[c]['point']) / math.pi * 180.0
            if math.isnan(angle):
                angle = get_angle_math(points[a]['point'], points[mid]['point'], points[c]['point'])
            return [angle]
        elif _type == 3:  # U
            mid_line = properties['mid_line']  # id w liscie w properties[0]/properties[1]
            directions_id = properties['idx_directions']
            idx = directions_id[mid_line]
            line_mid = lines_index[idx]['point_idx']
            line_a = lines_index[directions_id[mid_line - 1]]['point_idx']
            direction_a = lines_index[directions_id[mid_line - 1]]['direction']
            line_c = lines_index[directions_id[mid_line + 1]]['point_idx']
            direction_c = lines_index[directions_id[mid_line + 1]]['direction']
            a = first_point_which_is_good(direction_a, points[line_a[0]: line_a[1] + 1])
            c = first_point_which_is_good(direction_c, [x for x in reversed(points[line_c[0]: line_c[1] + 1])])
            b = points[line_mid[0]]['point']
            angle = angle_3points(a, b, c) / math.pi * 180.0
            b = points[line_mid[1]]['point']
            angle2 = angle_3points(a, b, c) / math.pi * 180.0
            angle_final = angle + angle2
            return [angle_final]
        else:
            angles = []
            for shape in properties['shapes']:
                idx = shape['idx']
                if idx == 0:
                    continue
                shape = shape['shape']
                if shape == "V":
                    idx = properties['idx_directions'][idx]
                    a = points[lines_index[idx - 1]['point_idx'][0]]['point']
                    mid_point = points[lines_index[idx]['point_idx'][0]]['point']
                    c = points[lines_index[idx]['point_idx'][-1]]['point']
                    angles.append(
                        angle_3points(a, mid_point, c) / math.pi * 180.0
                    )
                else:  # shape U
                    idx = properties['idx_directions'][idx]
                    a = points[lines_index[idx - 1]['point_idx'][0]]['point']
                    mid_point = points[lines_index[idx]['point_idx'][0]]['point']
                    c = points[lines_index[idx]['point_idx'][-1]]['point']
                    new_c = points[lines_index[idx + 1]['point_idx'][0]]['point']
                    xxx = angle_3points(a, mid_point, c) / math.pi * 180.0
                    if math.isnan(xxx):
                        xxx=0
                    bbb = angle_3points(mid_point, c, new_c) / math.pi * 180.0
                    if math.isnan(bbb):
                        bbb=0
                    print(xxx)
                    angle = (xxx +
                            bbb)
                    angles.append(angle)
            return angles


def advanced_analysis_S(lines):
    """
    Analyzes the shape of a hit
    """
    starter_direction = lines[0]['direction']
    directions = [starter_direction]
    id_directions = [0]
    previous = starter_direction
    for idx, line in enumerate(lines):
        if line == lines[0]:
            continue
        if line['direction'] in extend_direction(starter_direction) and line['direction'] in extend_direction(previous):
            # jeden kierunek powiedzmy
            # na pewno jest tutaj minimum 3 kreski, warunek z wcześniej, dwie liczy normalnie
            # czyli jak mamy 3 to możliwości są
            # np.  [N NW N], [N NW NE], 1 opcja to uboga S, nie musimy sie martwic o więcej zgieć ani dzielić
            # tutaj mozna liczyc dwa katy , to robak powykrzywiany
            previous = line['direction']
            continue

        else:
            # jest jakaś nawrotka -> Robi się U lub L
            # jedna nawrtoka -> L lub U zależy czy dwa pod skos czy inna kombinacja, skos -> U
            # reszta L lub podobne
            # ogólnie jedna nawrotka to będzie L lub U
            # Dwie nawrotki to U lub kształt S
            # jest jakies ktore sie nie zgadza wiec dodajemy do kierunkow
            directions.append(line['direction'])
            id_directions.append(idx)
            # oraz dajemy nowy ogólny kierunek
            starter_direction = line['direction']
        previous = line['direction']
    typ = len(directions)
    if typ == 1:
        return 1, {"directions": directions}
        # tutaj ten specyficzny robak, prostolinijny prawie ze
        # dziwna S też możliwa [NE,NW,NE itd..
    elif typ == 2:
        return 2, {"directions": directions}

        # L musi być
        # lub specyficzne U
        # w obu przypadkach, tam gdzie jest załamanie tam bedzie liczony kat, wiec nie ma to dla nas wiekszego znaczenia
        # więc przyda nam się jakieś ID tego skad liczyc kat
    else:
        # FIXME możliwe, że trzeba bedzie uzupełnić czymś jeszcze
        U_TEMPLATE = [
            ["N", "E", "S"],
            ["N", "W", "S"],
            ["S", "E", "N"],
            ["S", "W", "N"],
            ["E", "N", "W"],
            ["E", "S", "W"],
            ["W", "S", "E"],
            ["W", "N", "E"],
            ["N", "SE", "E"],
            ["W", "S", "NE"],
            ["W", "S", "SE"],
            ["W", "N", "NE"],
            ["W", "N", "SE"],
            ["E", "S", "NW"],
            ["E", "S", "SW"],
            ["E", "N", "NW"],
            ["E", "N", "SW"],
            ["S", "E", "NE"],
            ["S", "E", "NW"],
            ["S", "W", "NE"],
            ["S", "W", "NW"],
            ["N", "E", "SE"],
            ["N", "E", "SW"],
            ["N", "W", "SE"],
            ["N", "W", "SW"],
            # to bardziej L, chyba nawet tutaj nie dojdzie taka kombinacja, SE-> E złączy w jedno i potraktuje jako 2
            # czyli w/w L
            ["N", "SW", "W"],
            ["SE", "E", "NE"],
            ["SW", "W", "NW"],
            ["NE", "E", "SE"],
            ["NW", "W", "SW"]
        ]
        find_u = 0
        mid_line = []
        for i in range(0, len(directions)):
            short = directions[i:i + 3]
            if short in U_TEMPLATE:
                find_u += 1
                mid_line.append(i + 1)


        V_TEMPLATE = [
            ["NW", "SW"],
            ["SW", "NW"],
            ["NW", "NE"],
            ["NE", "NW"],
            ["SW", "SE"],
            ["SE", "SW"],
            # takie poniżej układy też mogą być, dlatego że algorytm szuka załamań, to nie oznacza,
            # że one są obok siebie, jest to niemożliwe wręcz, ale tutaj kreska pomiedzy nimi bedzie
            # punktem zaczepnym załamania
            # pytanie czy któryś z tych w ogóle będzie takim przypadkiem
            ["NW", "SE"],
            ["SE", "NW"],
            ["NE", "SW"],
            ["SW", "NE"]
        ]
        mid_line = []
        find_u = 0
        find_v = 0
        for i in range(0, len(directions)):
            short = directions[i:i + 3]
            if short in U_TEMPLATE:
                mid_line.append({"shape": "U", "idx": i + 1})
                find_u += 1
                continue
            short = directions[i:i + 2]
            if short in V_TEMPLATE:
                mid_line.append({"shape": "V", "idx": i})
                find_v += 1
        if find_v == 0:
            return 3, {"directions": directions, "idx_directions": id_directions, "mid_line": mid_line[0]}
        return 4, {"directions": directions, "idx_directions": id_directions, "shapes": mid_line}


def set_line_v2_step4b_experimental(lines_index, points_e, list_of_point_index, threshold=1, percent_threshold=0.1):
    """
    Other way to line_strength
    """
    counter = collections.Counter()
    for line_index in lines_index:
        counter[line_index['direction']] += 1
    if len(counter) != 2:
        return lines_index
    directions = counter.most_common(2)
    one = directions[0][0]
    two = directions[1][0]
    if one in extend_direction(two):
        counter_points = collections.Counter()
        for point in points_e:
            counter_points[point['main_direction']] += 1
        THRESHOLD_GOOD = False

        i = 0
        for point in points_e:
            if point['main_direction'] in counter_points.most_common(2)[0][0] or \
                    point['main_direction'] in counter_points.most_common(2)[1][0]:
                i += 1
        wrong_pixel_percent = 1 - (i / len(points_e))
        if i + threshold >= len(points_e) or wrong_pixel_percent <= percent_threshold:
            THRESHOLD_GOOD = True
        if len(counter_points) == 2 or THRESHOLD_GOOD:
            # w jednym kierunku jest potencjalnie
            # teraz różnica miedzy rzeczywistym S -> SW -> S a przeskokami
            bigger_line = []
            for idx, point_idx in enumerate(list_of_point_index):
                if point_idx[1] - point_idx[0] > 3:
                    bigger_line.append({"idx": idx, "point_idx": point_idx})
            if len(bigger_line) < 2:
                return [{"point_idx": (0, len(points_e) - 1), "direction": counter_points.most_common(1)[0]}]

    return lines_index
