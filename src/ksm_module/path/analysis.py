import ksm_module.path.steps_alghoritm_new as san
from typing import Tuple, Callable

from credo_cf import NKG_PATH, store_png, IMAGE, ID
from credo_cf.classification.preprocess.nkg_processings import nkg_make_track
from example.ksm import nkg_mark_hit_area_mod
from ksm_module.path.common import *
from ksm_module.path.steps_alghoritm import *


def check_line_straight(hit, threshold=1, threshold_percent=0.2) -> List[int]:
    """
    return: list with angle of 180 or an empty list if the line is not straight
    """
    counter = collections.Counter()
    for point in hit:
        counter[point['main_direction']] += 1

    if len(counter) == 1:
        return [180]
    else:
        if len(counter) > 3:
            return []
        main_direction = counter.most_common(1)[0][0]
        i = 0
        for point in hit:
            if main_direction in point['main_direction'] or point['main_direction'] in main_direction:
                i += 1
        wrong_pixel_percent = 1 - (i / len(hit))
        if i + threshold >= len(hit) or wrong_pixel_percent < threshold_percent:
            return [180]
        else:
            return []


def check_line_straight_worms(hit, threshold=1, threshold_percent=0.2) -> List[int]:
    """
    return: list with angle of 180 or an empty list if the line is not straight
    """
    counter = collections.Counter()
    for point in hit:
        counter[point['main_direction']] += 1

    if len(counter) == 1:
        return True
    else:
        main_direction = counter.most_common(1)[0][0]
        i = 0
        for point in hit:
            if main_direction == point['main_direction']:
                i += 1
        wrong_pixel_percent = 1 - (i / len(hit))
        if i + threshold >= len(hit) or wrong_pixel_percent < threshold_percent:
            return True
        else:
            return False


def find_adjacent_points(point: Tuple) -> List[Tuple]:
    """
    return a list with the positions of adjacent points
    """
    new_list = []
    for i in range(-1, 2):
        for j in range(-1, 2):
            new_list.append((point[0] + i, point[1] + j))
    new_list.remove(point)
    return new_list


def extend_path_information(path: List[Tuple]) -> List[dict]:
    """
    return dictionary with
    "adjacent_points" - List[Tuple] with 8 elements
    "directions" - "List[str] in which directions it has connections with points
    "main_direction" - base pixel to pixel direction
    """
    ep = []
    for i in path:
        ep.append({"point": i, "adjacent_points": find_adjacent_points(i)})
    for idx, point_dict in enumerate(ep):
        directions = []
        for point in path:
            if point_dict['point'] == point:
                continue
            if point in point_dict['adjacent_points']:
                direction_index = point_dict['adjacent_points'].index(point)
                directions.append(get_direction_from_index(direction_index))
        point_dict['directions'] = directions
        if idx + 1 < len(ep):
            direction_index = point_dict['adjacent_points'].index(ep[idx + 1]['point'])
            point_dict['main_direction'] = get_direction_from_index(direction_index)
        else:
            point_dict['main_direction'] = opposite_direction(point_dict['directions'][0])
    return ep


def check_line_straight_ideal_one_pixel(points: List[dict]) -> bool:
    starter = points[0]['main_direction']
    for point in points:
        if point['main_direction'] != starter:
            return False
    return True


def add_new_points(points: List[dict]) -> List[dict]:
    points = set_potential_expansion(points)
    counter = 0
    for idx, point in enumerate(points):
        counter += set_new_points(point, points, idx + 1, len(points), 1)
    end = len(points) - counter
    for idx, point in enumerate(reversed(points)):
        if idx >= end:
            break
        set_new_points(point, points, (-1) * (idx + 1), (-1) * len(points), -1)
    return points


def set_new_points(point, points, start, stop, step) -> int:
    direction_base = point['main_direction']
    direction_find: str = ""
    if len(direction_base) == 1:
        find = False
        for ii in range(start, stop, step):
            i = points[ii]
            if find:
                break
            for d in i['directions']:
                if d == direction_base or d == opposite_direction(direction_base):
                    continue
                else:
                    direction_find = d
                    find = True
                    break
    else:
        direction_find = direction_base
    for i in point['potential_expansion']:
        if i in direction_find:
            point['new_point'] = [point['point'], point['adjacent_points'][get_index_from_direction(i)]]
            return 1
    return 0


def extend_path_to_2_pixel(more_info_path: List[dict]):
    new_points = []
    if check_line_straight_ideal_one_pixel(more_info_path):
        for point in more_info_path:
            new_points.append([point['point']])
    else:
        new_full_points = add_new_points(more_info_path)
        for point in new_full_points:
            new_points.append(point['new_point'])
    return new_points


def find_the_best_points(points_s: List, points_e) -> List:
    best_value = []
    for i in points_s:
        for j in points_e:
            a, b = i
            c, d = j
            if a - c == b - d or a == c or b == d:
                best_value.append([i, j])
    if best_value:
        if len(best_value) == 1:
            return best_value[0]
        max_value = 0
        element_to_find = best_value[0]
        for i in best_value:
            val = (i[0][0] - i[1][0]) ** 2 + (i[0][1] - i[1][1]) ** 2
            val = math.sqrt(val)
            if val > max_value:
                max_value = val
                element_to_find = i
        return element_to_find
    return []


def find_mid_points(start, stop, mid_list, mid_original):
    best_value = []
    for list_of_mid in mid_list:
        for point in list_of_mid:
            if any([
                all([point[0] - start[0] == point[1] - start[1], point[0] - stop[0] == point[1] - stop[1]]),
                all([point[0] == start[0], point[0] == stop[0]]),
                all([point[1] == start[1], point[1] == stop[1]])
            ]):
                best_value.append(point)

    if len(best_value) == 1:
        return best_value[0]
    return mid_original


def find_the_three_points_in_two_pixel(path) -> List:
    start_list = path[:2]
    stop_list = path[-2:]
    start, stop, idx_start, idx_stop = find_start_stop_combination_linear(start_list, stop_list, len(path))
    idx_stop = idx_stop - 2  # translate index [0,1] for ...,-2,-1]
    new_path = path[idx_start + 1:idx_stop]
    length = len(new_path)
    if length == 0:
        return [(0, 0), (0, 1), (0, 2)]  # zachowanie 180
    mid_original = new_path[length // 2][0]
    if length % 2 == 1:
        n = length // 2
        mid_list = new_path[n - 1:n + 2]
    else:
        n = length // 2
        mid_list = new_path[n - 1:n + 1]

    mid = find_mid_points(start, stop, mid_list, mid_original)
    return [start, mid, stop]


def find_start_stop_combination_linear(start: List, stop: List, length) -> List:
    best_value = []
    _max = 0
    _min = 0
    for idx, list_of_starter in enumerate(start):
        for i in list_of_starter:
            for idy, list_of_stop in enumerate(stop):
                for j in list_of_stop:
                    a, b = i
                    c, d = j
                    if a - c == b - d or a == c or b == d:
                        best_value.append([i, j, idx, idy])
    if best_value:
        if len(best_value) == 1:
            return best_value[0]
        max_value = 0
        element_to_find = best_value[0]
        for i in best_value:
            val = (i[0][0] - i[1][0]) ** 2 + (i[0][1] - i[1][1]) ** 2
            val = math.sqrt(val)
            if val > max_value:
                max_value = val
                element_to_find = i
        return element_to_find
    if length > 4:
        return [start[1][0], stop[0][0], 1, 0]
    else:
        return [start[0][0], stop[1][0], 0, 1]


def set_line(list_idx, points_e, threshold: int = 2, threshold_percent: float = 0.2):
    """
    1. Podlicza z danego zakresu ilość wystąpień
    """
    bend = False
    bend_two = True
    earlier_start = 0
    new_list = []
    for idx in list_idx:
        counter = collections.Counter()
        _start, _stop = idx
        if (_stop - _start + 1) <= 2 and _stop != len(points_e) - 1:
            if bend_two:
                earlier_start = _start
                bend_two = False
            # załamanie pixelowe, po co sprawdzać? lećmy dalej
            bend = True  # aby zapisany wcześniej w pamieci start nadpisał z tego od nowa
            continue
        else:
            bend_two = True
        if bend:
            _start = earlier_start
            bend = False
        for j in range(_start, _stop):
            counter[points_e[j]['main_direction']] += 1

        if len(counter) == 1:  # Linia prosta
            new_list.append(idx)
        else:
            main_direction = counter.most_common(1)[0][0]
            good_direction = 0
            for point_idx in range(_start, _stop):
                point = points_e[point_idx]
                if main_direction in point['main_direction'] or point['main_direction'] in main_direction:
                    good_direction += 1
            wrong_pixel_percent = 1 - (good_direction / (_stop - _start))
            if good_direction + threshold >= (_stop - _start) or wrong_pixel_percent < threshold_percent:
                new_list.append((_start, _stop))
            else:
                bend = True
                earlier_start = _start
    return new_list


def find_angle_in_path(points_e: List[dict]) -> List[dict]:
    list_of_point_index = []
    actual_direction = points_e[0]['main_direction']
    starter_index = 0
    for idx, point in enumerate(points_e):
        if point['main_direction'] != actual_direction:
            list_of_point_index.append((starter_index, idx))
            starter_index = idx
            actual_direction = point['main_direction']
    list_of_point_index.append((starter_index, len(points_e) - 1))
    line_index = set_line_v2(list_of_point_index)
    line_index2 = set_line_v2_step3(line_index, points_e)
    line_index3a = set_line_v2_step2(line_index2)
    line_index3 = set_line_v2_step3(line_index3a, points_e)
    line_index4 = set_line_v2_step4(line_index3)
    EXPERIMENTAL_STEP = True
    if EXPERIMENTAL_STEP:
        line_index4 = set_line_v2_step4b_experimental(line_index4, points_e, list_of_point_index)
    angle = set_line_v2_step5_ver1(line_index4, points_e)
    angle2 = set_line_v2_step5_ver3(line_index4, points_e)
    if len(angle2) > 3:
        angle2_tmp = check_line_straight(points_e, threshold=2, threshold_percent=0.1)
        if len(angle2_tmp) > 0:
            angle2 = angle2_tmp
    return angle, angle2


def find_angle_in_path_v2(points_e: List[dict]) -> List[dict]:
    line_list = san.find_straight_lines(points_e)
    line_dict = san.set_directions_from_list_of_set(line_list, points_e)
    line_dict2 = san.merge_lines_with_hop(line_dict)
    line_dict3 = san.merge_custom_3_hop(line_dict2)
    line_dict4 = san.merge_hop_at_the_end(line_dict3)
    line_dict5 = san.merge_lines_with_hills(line_dict4)
    line_dict6 = san.merge_adjacent_in_the_same_direction(line_dict5)
    line_dict7 = san.merge_rest(line_dict6)
    angle = set_line_v2_step5_ver3(line_dict7,points_e)
    print(line_dict2)
    print(line_dict2)
    return angle,angle


def analyse_path_ksm_new(hit: dict, source_path: str = NKG_PATH) -> dict:
    path = hit.get(source_path)
    if len(path) < 3:
        return []
    hit['more_info_path'] = extend_path_information(path)
    # hit['new_path'] = extend_path_to_2_pixel(hit['more_info_path'])
    hit['angles_in_path'], hit['angles_in_path_2'] = find_angle_in_path(hit['more_info_path'])
    hit['2angles_in_path'], hit['2angles_in_path_2'] = find_angle_in_path_v2(hit['more_info_path'])
    return hit['angles_in_path_2']


def measure_angle_ksm(hits, fn: str, post_fix="",
                      processing_mark: Tuple[Callable, str] = (nkg_mark_hit_area_mod, 'final'),
                      analyse_function: Callable = analyse_path_ksm_new
                      ):
    func, name_mask = processing_mark
    for h in hits:
        print(h['id'])
        func(h)
        nkg_make_track(h, scale=1, downscale=False, skeleton_method='zhang', name_mask=name_mask)
        angle = 0
        angless = "0"
        if h.get(NKG_PATH):
            angles = analyse_function(h, NKG_PATH)
            if len(angles):
                angle = angles[0]
                angless = ""
                for idx, anglee in enumerate(angles):
                    angless += str(int(anglee))
                    angless += "_"
        if math.isnan(angle):
            angle = 0
        print("angle: {}".format(angle))
        show_image(h)

        store_png('../tmp/credo_two_points' + post_fix, [fn], '%s_%s' % (angless, str(h.get(ID))),
                  h.get(IMAGE))


def show_image(hit):
    x = []
    y = []
    import matplotlib.pyplot as plt
    for _x, _y in hit.get('nkg_path'):
        x.append(_x)
        y.append(_y)
    plt.plot(x, y, 'ro')
    angless = ""
    angles_v1 = ""
    angles_new=""
    if "2angles_in_path_2" in hit:
        for ifx,anglee in enumerate(hit['2angles_in_path_2']):
            angles_new += str(int(anglee))
            if ifx == len(hit['2angles_in_path_2']) - 1:
                break
            angles_new += "_"

    if "angles_in_path" in hit:
        if type(hit['angles_in_path']) ==list:
            for idx, anglee in enumerate(hit['angles_in_path']):
                angles_v1 += str(int(anglee))
                if idx == len(hit['angles_in_path_2']) - 1:
                    break
                angles_v1 += "_"
        else:
            angles_v1 += str(int(hit['angles_in_path']))
    if "angles_in_path_2" in hit:
        for idx, anglee in enumerate(hit['angles_in_path_2']):
            angless += str(int(anglee))
            if idx == len(hit['angles_in_path_2']) - 1:
                break
            angless += "_"
    for i_x, i_y in zip(x, y):
        plt.text(i_x, i_y, '({}, {})'.format(i_x, i_y))
    plt.title("ID: {}, v1: {}, v2: {} new: {}".format(hit['id'], angles_v1,
                                                          angless,angles_new))
    plt.show()


######################
@DeprecationWarning
def find_start_point_with_sort(path) -> List:
    sorted_list = sort_with_path(path)
    return find_the_best_points([sorted_list[0], sorted_list[1]], [sorted_list[-2], sorted_list[-1]])


@DeprecationWarning
def sort_with_path(path: List[Tuple]) -> List[Tuple]:
    sort_path = sorted(path)
    start_points = None
    for point in sort_path:
        x, y = point
        neighbor_points = []
        for i in range(-1, 2):
            for j in range(-1, 2):
                neighbor_points.append((x + i, y + j))
        counter = 0
        for ii in sort_path:
            if ii in neighbor_points:
                counter += 1
        if counter == 2:
            start_points = point
            break

    new_path = [start_points]
    copy_path = path.copy()
    """
    Brak obsłużonego przypadku jeżeli są dwa sąiady po skosie lub jakiś inny bug wyszukanej drogi
    """
    point = start_points
    while copy_path:
        copy_path.remove(point)
        if len(copy_path) == 0:
            break
        x, y = point
        neighbor_points = []
        for i in range(-1, 2):
            for j in range(-1, 2):
                neighbor_points.append((x + i, y + j))
        nei_p = []
        for ii in copy_path:
            if ii in neighbor_points:
                nei_p.append(ii)
        if len(nei_p) == 1:  # jest tylko jeden sąsiad:
            new_path.append(nei_p[0])
            point = nei_p[0]
            # copy_path.remove(nei_p[0])
        elif len(nei_p) == 2:  # pierwszeństwo mają punkty boczne, z nich można potem tez bocznie zejsc
            # Boczny będzie miał potwarzająćy się x lub y
            x, y = point
            for _point in nei_p:
                _x, _y = _point
                if x == _x or y == _y:
                    new_path.append(_point)
                    point = _point
                    # copy_path.remove(point)
                    break
            else:
                new_path.append(nei_p[0])
                point = nei_p[0]
        else:
            """ Tutaj jakaś rekurencja pasuje aby szukać rozgałęzień"""
            print("MAMY PROBLEM")
            return path
    return new_path


@DeprecationWarning
def find_the_best_point_three(path, points_start_stop) -> List:
    start = points_start_stop[0]
    index_start = 0
    index_stop = 0
    for idx, elem in enumerate(path):
        if collections.Counter(elem) == collections.Counter(points_start_stop[0]):
            index_start = idx
        if collections.Counter(elem) == collections.Counter(points_start_stop[1]):
            index_stop = idx
    new_path = path[index_start:index_stop + 1]
    length_new_path = len(new_path)
    if length_new_path == 1:
        return new_path[0]
    if length_new_path > 4:
        new_path = new_path[length_new_path // 2 - 1:length_new_path // 2 + 2]  # 3 in middle points
    elif length_new_path == 4:
        new_path = new_path[1:3]  # 2 mid

    best_value = []
    for idx, elem in enumerate(new_path):
        if elem[0] - start[0] == elem[1] - start[1] or elem[0] == start[0] or elem[1] == start[1]:
            best_value.append(idx)
    if best_value:
        return new_path[best_value[0]]
    # Czy ma znaczenie który punkty, skoro reszta idealna? DLa robaków pewnie ma, dla kreski nie
    # a co jak nie znajdzie takiego? to na jakiej podstawie szukać
    # na razie damy środkowy
    return new_path[len(new_path) // 2]


@DeprecationWarning
def find_start_stop_points_in_all_pitagoras(path) -> List:
    list_of_value = []
    list_of_value2 = []
    experimental = sorted(path)
    for i in path:
        list_of_value.append({"value": math.sqrt(i[0] ** 2 + i[1] ** 2), "coord": i})
    for i in experimental:
        list_of_value2.append({"value": math.sqrt(i[0] ** 2 + i[1] ** 2), "coord": i})
    print(list_of_value2)
    sorted_list = sorted(list_of_value, key=lambda k: k['value'])
    print(sorted_list)
    list_a = []
    list_b = []
    for idx, a in enumerate(sorted_list):
        val, cod = a.values()
        if idx < len(sorted_list) // 2:
            list_a.append(cod)
        elif idx > len(sorted_list) // 2:
            list_b.append(cod)
    # return find_the_best_points([sorted_list[0]['coord'], sorted_list[1]['coord']], [sorted_list[-2]['coord'],
    # sorted_list[-1]['coord']])
    return find_the_best_points(list_a, list_b)
