import math
from typing import List

SLANT = ['SE', 'SW', 'NE', 'NW']
STRAIGHT = ['E', 'W', 'S', 'N']


def set_potential_expansion(points: List[dict]) -> List[dict]:
    for point in points:
        point['potential_expansion'] = potential_expansion_from_direction(point['main_direction'])
    return points


def perpendicular_directions(direction: str) -> List[str]:
    if direction == "E" or direction == "W":
        return ["N", "S"]
    if direction == "S" or direction == "N":
        return ["E", "W"]
    if direction == "NE":
        return ["SE", "NW"]
    if direction == "NW":
        return ["SW", "NE"]
    if direction == "SE":
        return ["NE", "SW"]
    if direction == "SW":
        return ["NW", "SE"]


def potential_expansion_from_direction(direction: str) -> List[str]:
    if direction == "E" or direction == "W":
        return ["N", "S"]
    if direction == "S" or direction == "N":
        return ["E", "W"]
    if direction == "NE":
        return ["N", "E"]
    if direction == "NW":
        return ["N", "W"]
    if direction == "SE":
        return ["E", "S"]
    if direction == "SW":
        return ["W", "S"]
    raise Exception("Error with potential_expansion, wrong value")


def opposite_direction(direction: str) -> str:
    if direction == "E":
        return "W"
    if direction == "W":
        return "E"
    if direction == "NE":
        return "SW"
    if direction == "SW":
        return "NE"
    if direction == "S":
        return "N"
    if direction == "N":
        return "S"
    if direction == "SE":
        return "NW"
    if direction == "NW":
        return "SE"
    raise Exception("Error with opposite_direction, wrong value")


def get_direction_from_index(index: int) -> str:
    if index == 0:
        return "NW"
    if index == 1:
        return "N"
    if index == 2:
        return "NE"
    if index == 3:
        return "W"
    if index == 4:
        return "E"
    if index == 5:
        return "SW"
    if index == 6:
        return "S"
    if index == 7:
        return "SE"
    raise Exception("Error with set_direction, wrong value")


def get_index_from_direction(direction: str) -> int:
    return ["NW", "N", "NE", "W", "E", "SW", "S", "SE"].index(direction)


def extend_direction(direction: str) -> List[str]:
    if direction == "S":
        return ["SW", "S", "SE"]
    if direction == "N":
        return ["NW", "N", "NE"]
    if direction == "E":
        return ["NE", "E", "SE"]
    if direction == "W":
        return ["NW", "W", "SW"]
    if direction == "SW":
        return ["W", "SW", "S"]
    if direction == "NW":
        return ["W", "NW", "N"]
    if direction == "SE":
        return ["S", "SE", "E"]
    if direction == "NE":
        return ["N", "NE", "E"]


def first_point_which_is_good(direction: str, points: List[dict]):
    for point_dict in points:
        if point_dict['main_direction'] == direction:
            return point_dict['point']
    raise Exception("unexpected content first_point_which_is_good")


def get_angle_math(a, b, c):
    """
    Angle from the math library
    """
    ang = math.degrees(math.atan2(c[1] - b[1], c[0] - b[0]) - math.atan2(a[1] - b[1], a[0] - b[0]))
    return ang + 360 if ang < 0 else ang
