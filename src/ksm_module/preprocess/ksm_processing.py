from typing import List, Tuple

from credo_cf import find_all_maximums, GRAY, np


def ksm_mark_hit_area(detection: dict, recursion_func=None, kernel: List[Tuple[int, int]] = None):
    detection['ksm_avg'] = universal_filter_ksm(detection, mask=AVERAGING_FILTER)
    ret = find_all_maximums(detection['ksm_avg'], recursion_func=recursion_func, kernel=kernel, spread_on_flat=False,
                            max_maximums=200)
    find_median = ret['values']
    core_median = find_median[len(find_median) // 2]
    # threshold = (find_median[0] - core_median) // 10 + core_median
    threshold = core_median + 2
    ret2 = find_all_maximums(detection.get(GRAY), recursion_func=recursion_func, kernel=kernel, spread_on_flat=True,
                             threshold=threshold, max_maximums=200)
    detection['nkg_mask_mod'] = ret2['mask']

    detection['ksm_mask_avg'] = universal_filter_ksm(detection, 'nkg_mask_mod', mask=AVERAGING_FILTER)
    for i in range(0, 2):
        detection['ksm_mask_avg'] = universal_filter_ksm(detection, 'ksm_mask_avg', mask=AVERAGING_FILTER)

    detection['mark'] = mark_all(detection['ksm_mask_avg'], treshold=5)
    detection['cut_hit_mark'] = cut_hit_in_mark(detection.get(GRAY), detection['mark'])
    threshold = set_treshold(detection.get('cut_hit_mark'))
    ret2 = find_all_maximums(detection.get('cut_hit_mark'), recursion_func=recursion_func, kernel=kernel,
                             spread_on_flat=True, threshold=threshold, max_maximums=200)
    detection['final'] = ret2['mask']

    return detection


def cut_hit_in_mark(ret, ret_mark):
    l = len(ret)
    new_ret = []
    for i in range(0, l):
        list_tmp = []
        for j in range(0, l):
            if ret_mark[i][j] == -55:
                list_tmp.append(ret[i][j])
            else:
                list_tmp.append(0)
        new_ret.append(list_tmp)
    new_ret = np.array(new_ret)
    return new_ret


def mark_all(ret, treshold: int = 0):
    l = len(ret)
    new_ret = []
    for i in range(0, l):
        list_tmp = []
        for j in range(0, l):
            if ret[i][j] > treshold:
                list_tmp.append(-55)
            else:
                list_tmp.append(0)
        new_ret.append(list_tmp)
    new_ret = np.array(new_ret)
    return new_ret


def set_treshold(ret) -> int:
    """
        FIXME
    """
    (unique, counts) = a = np.unique(ret, return_counts=True)
    if a[0][0] == 0:
        unique = unique[1:]
        counts = counts[1:]
    ilosc_pixeli = sum(counts)
    # najwięcej_pixeli = max(counts)
    pozycja_w_liscie = np.where(
        counts == max(counts))  # średnia dla szum, potencjalnie, lecz co w przypadku dużego suzmu?
    zakres = 4
    number = unique[pozycja_w_liscie[0][0]] + zakres
    return number


HIGH_PASS_FILTRER = [[-1, -1, -1],
                     [-1, 9, -1],
                     [-1, -1, -1]]
MY_HIGH_PASS_FILTRER = [[-1, -1, -1],
                        [-1, 20, -1],
                        [-1, -1, -1]]

AVERAGING_FILTER = [[1, 1, 1],
                    [1, 1, 1],
                    [1, 1, 1]]

AVERAGING_FILTER_V2 = [[1, 1, 1],
                       [1, 2, 1],
                       [1, 1, 1]]
HP2 = [[1, -2, 1],
       [-2, 5, -2],
       [1, -2, 1]
       ]


def universal_filter_ksm(detection: dict, source='gray', mask=None):
    if mask is None:
        mask = HIGH_PASS_FILTRER
    ret = detection.get(source)
    side_size = int(len(mask) / 2)
    size = len(mask)
    length = len(ret[0])
    list_tmp = []
    new_ret = []
    # lewy róg
    list_tmp.append(int((ret[0][0] * mask[1][1] + ret[0][1] * mask[1][2] + ret[1][0] * mask[2][1] + ret[1][1] *
                         mask[2][2]) / 4))
    ####góra
    for i in range(1, length - side_size):
        list_tmp.append(int((ret[0][i] * mask[1][1] +
                             ret[0][i - 1] * mask[1][0] +
                             ret[0][i + 1] * mask[1][2] +
                             ret[1][i] * mask[2][1] +
                             ret[1][i - 1] * mask[2][0] +
                             ret[1][i + 1] * mask[2][2]) / 6))
    ####prawy róg
    list_tmp.append(int(
        (ret[0][-1] * mask[1][1] +
         ret[0][-2] * mask[1][0] +
         ret[1][-1] * mask[2][1] +
         ret[1][-2] * mask[2][0]) / 4))

    new_ret.append(list_tmp)
    list_tmp = []
    for i in range(side_size, length - side_size):
        # list_tmp.append(ret[i][0])        #przystosować do side_size!! dziala tylko na 3x3
        # list_tmp.append(0)
        list_tmp.append(int((ret[i][0] * mask[1][1] +
                             ret[i][1] * mask[1][2] +
                             ret[i + 1][0] * mask[0][1] +
                             ret[i + 1][1] * mask[0][2] +
                             ret[i - 1][0] * mask[2][1] +
                             ret[i - 1][1] * mask[2][2]) / 6))
        for j in range(side_size, length - side_size):
            counter = 0
            sum = 0
            ii = i - 1
            jj = j - 1
            for k in range(0, size):
                for l in range(0, size):
                    if mask[k][l] != 0: counter += 1
                    sum += mask[k][l] * ret[ii][jj]
                    jj = jj + 1
                ii = ii + 1
                jj = j - 1
            avg = int(sum / (size * size))
            list_tmp.append(avg)
        # list_tmp.append(ret[i][length-1])        #przystosować do side_size!! dzila tylko na 3x3
        # list_tmp.append(0)
        list_tmp.append(int((ret[i][-1] * mask[1][1] +
                             ret[i][-2] * mask[1][0] +
                             ret[i + 1][-1] * mask[0][1] +
                             ret[i + 1][-2] * mask[0][0] +
                             ret[i - 1][-1] * mask[2][1] +
                             ret[i - 1][-2] * mask[2][0]) / 6))
        new_ret.append(list_tmp)
        list_tmp = []
    # new_ret.append(ret[-1])
    list_tmp = []
    # lewy róg dolny
    list_tmp.append(int((ret[-1][0] * mask[1][1] + ret[-1][1] * mask[1][2] + ret[-2][0] * mask[0][1] +
                         ret[-2][1] * mask[0][2]) / 4))
    ####dół
    for i in range(1, length - side_size):
        list_tmp.append(int((ret[-1][i] * mask[1][1] +
                             ret[-1][i - 1] * mask[1][0] +
                             ret[-1][i + 1] * mask[1][2] +
                             ret[-2][i] * mask[0][1] +
                             ret[-2][i - 1] * mask[0][0] +
                             ret[-2][i + 1] * mask[0][2]) / 6))
    ####prawy róg dolny
    list_tmp.append(int(
        (ret[-1][-1] * mask[1][1] +
         ret[-1][-2] * mask[1][0] +
         ret[-2][-1] * mask[0][1] +
         ret[-2][-2] * mask[0][0]) / 4))
    new_ret.append(list_tmp)
    newRetNp = np.array(new_ret)
    return newRetNp
